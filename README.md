# Sample Expense report to test out Rspec in depth
__Stack__:
- Sinatra
- Rack::Test
- Sqlite for the test DB
- nothing else really!

__Sequel__ 
The test DB is in SQLite.
`require 'sequel'
 DB = Sequel.sqlite
 DB.create_table(:name){ Type:name }
 DB[:table_name].insert('name: RSpec')
`
